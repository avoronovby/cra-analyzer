const sortF = (a, b) => {
  if (a.size > b.size) return -1;
  if (a.size < b.size) return 1;
  if (a.size === b.size) return 0;
};

export { sortF };
