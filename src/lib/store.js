import fileSize from "filesize";
import data from "../bundle-stats.json";
import { sortF } from "./utils";
let { modules, chunks } = data;

console.log(data);

const totalSize = modules.reduce((acc, { size }) => {
  return acc + parseInt(size);
}, 0);

chunks = chunks.filter((chunk) => {
  chunk.sizeText = fileSize(chunk.size);
  let { modules } = chunk;
  modules.sort(sortF).map((module) => {
    module.sizeText = fileSize(module.size);
    return module;
  });
  return chunk.size > 0;
});

const store = {
  chunks: chunks.sort(sortF),
  totalSize: totalSize,
};

export default store;
