import ModulesTree from "../module/modulesTree";
import ModuleItem from "../module/moduleItem";
import s from "./index.module.css";
import ChunksList from "../chunks/chunksList";
import lodash from 'lodash';

const Main = () => {
  return (
    <div className={s.container}>
      <ModulesTree>
        <ChunksList />

        {/* {store.modules.map(({ name, size }) => {
          return (
            <ModuleItem
              key={name + size}
              size={size}
              name={name}
              proportion={size / store.totalSize}
            />
          );
        })} */}
      </ModulesTree>
    </div>
  );
};

export default Main;
