import { useState } from "react";
import store from "../../../lib/store";
import ModuleItem from "../../module/moduleItem";
import s from "./idnex.module.css";

const ChunkItem = ({ chunk }) => {
  const [hovered, setHovered] = useState(false);
  const { modules } = chunk;
  console.log(chunk);
  const onMouseEnter = () => {
    setHovered(true);
  };
  return (
    //   onMouseEnter={onMouseEnter}
    <div className={s.container}>
      <p>{chunk.files[0] ? chunk.files[0] : `chunk`}</p>
      <p className="size">{hovered && `Chunk size: ${chunk.size}`}</p>
      {modules.map((module) => {
        return (
          <ModuleItem {...module} proportion={module.size / store.totalSize} />
        );
      })}
    </div>
  );
};

export default ChunkItem;
