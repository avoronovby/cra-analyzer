import store from "../../../lib/store";
import ChunkItem from "../chunksItem";
import s from "./index.module.css";

const ChunksList = () => {
  const { chunks } = store;

  return (
    <div className={s.container}>
      {chunks.map((chunk) => {
        return <ChunkItem chunk={chunk} key={chunk.id} />;
      })}
    </div>
  );
};

export default ChunksList;
