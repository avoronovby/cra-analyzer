import s from "./index.module.css";

const ModulesTree = ({ children }) => {
  return <div className={s.container}>{children}</div>;
};

export default ModulesTree;
