import s from "./index.module.css";

const ModuleItem = ({ sizeText, name, proportion }) => {
  const divStyle = {
    height: proportion + "%",
  };

  return (
    <div style={divStyle} className={s.container}>
      <p>Size: {sizeText}</p>
      <p>{name}</p>
    </div>
  );
};

export default ModuleItem;
